import React from 'react';
import './App.css';
import Navbar from './component/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home'
import Product from './pages/Product'
import Reports from './pages/Reports'

function App() {
  return (
    <div>
    <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/reports' component={Reports} />
          <Route path='/product' component={Product} />
        </Switch>
    </Router>
    </div>
  );
}

export default App;
